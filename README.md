# vim-trexfitter

Simple syntax highlighting in vim for [TRExFitter](https://gitlab.cern.ch/TRExStats/TRExFitter) config files. This doesn't detect the filetype, so you have to add `% vim: set ft=trexconf :` at the top of the file to enable highlighting.
