#!/bin/bash

if [ ! -d $HOME/.vim/ ]; then
    mkdir $HOME/.vim/
fi

if [ ! -d $HOME/.vim/ftplugin ]; then
    mkdir $HOME/.vim/ftplugin
fi

if [ ! -d $HOME/.vim/syntax ]; then
    mkdir $HOME/.vim/syntax
fi

cp -vi ftplugin/trexconf.vim $HOME/.vim/ftplugin/
cp -vi syntax/trexconf.vim $HOME/.vim/syntax/
