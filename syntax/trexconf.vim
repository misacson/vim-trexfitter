if exists("b:current_syntax")
    finish
endif

syn keyword trexTodo contained TODO FIXME XXX

syn match trexComment "^%.*" contains=trexTodo
syn match trexComment "\s%.*"ms=s+1 contains=trexTodo
syn region trexString start=+"+ skip=+\\\\\|\\"+ end=+"+ oneline
syn region trexString start=+"+ skip=+\\\\\|\\"+ end=+"+ oneline
syn match trexNumber "[a-zA-Z0-9]\@<![+-]*\d\+\.\?\d*"

syn match trexColon ":"
syn match trexKey "^[a-zA-Z]\+" nextgroup=trexColon
syn match trexOpt "^\s\+[a-zA-Z]\+" nextgroup=trexColon

hi def link trexComment Comment
hi def link trexTodo Todo
hi def link trexString String
hi def link trexNumber Constant
hi def link trexKey Identifier
hi def link trexOpt Type

let b:current_syntax = "trexconf"
